#!/usr/bin/env bash
aws ses send-templated-email  --cli-input-json file://$1/emailData.json --region us-west-2
