#!/usr/bin/env bash
aws ses delete-template --template-name $1 --region us-west-2
