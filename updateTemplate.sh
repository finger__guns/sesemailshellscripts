#!/usr/bin/env bash
aws ses update-template --cli-input-json file://$1 --region us-west-2
