#!/usr/bin/env bash
aws ses create-template --cli-input-json file://$1 --region us-west-2
